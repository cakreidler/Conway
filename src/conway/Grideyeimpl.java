package conway;

public class Grideyeimpl implements Grideye 
{
	public InfoObj lookatgrid(byte a, byte b, LifeGrid grid, InfoObj infobj) {
		// TODO Auto-generated method stub
		//This looks at a 3x3 area around the center cell
	
		int count = 0;
		//returns the status of the center cell
		boolean status = grid.getGridAloc((byte)(a % 8),(byte) (b % 8));
		//returns the total live cells in a 3x3 area
		count = Lifecount(a, b, grid);
		infobj.setCount(count);
		infobj.setStatus(status);
		//System.out.println(count + "count" + status + "Status");
		
		return infobj;
	}

	
	public int Lifecount(byte x, byte y, LifeGrid grid) {
		// TODO Auto-generated method stub
		int count = 0;
		byte eyestartx = (byte) (x - 1);
		byte eyestarty = (byte) (y - 1);
		byte eyelimitx = (byte) (x + 1);
		byte eyelimity = (byte) (y + 1);
		//(7,7)(0,7)(1,7)
		//(7,0)(0,0)(1, 0)
		//(7,1)(0,1)(1,1)
		for(byte by = eyestarty; by <= eyelimity; by++)
		{
			for(byte bx = eyestartx ; bx <= eyelimitx; bx++)
			{
				
				//System.out.print((byte)(bx % 8) + " " + (byte)(by % 8));
				if((bx == x) && (by == y))
				{
					//System.out.println("NOTGONNALOOK");
				}else if((grid.getGridAloc((byte)(bx % 8),(byte)(by % 8)) == true)) 
				{
					//System.out.print("DING");
					count += 1;
				}
				//System.out.println("");
			}
		}
		//System.out.println(count);
		return count;
	}

}
