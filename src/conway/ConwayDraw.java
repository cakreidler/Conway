package conway;

import javax.swing.*;
import java.awt.*;

public class ConwayDraw extends JPanel{
	
	boolean[][] paintbynumber = new boolean[8][8];
	
	
	public boolean[][] getPaintbynumber() {
		return paintbynumber;
	}


	public void setPaintbynumber(boolean[][] paintbynumber) {
		this.paintbynumber = paintbynumber;
	}
	
	@Override
	public Dimension getPreferredSize()
	{
		return new Dimension(800,800);
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		for(int y = 0; y < 8; y++)
		{
			for(int x = 0; x < 8; x++)
			{
				if(paintbynumber[x][y] == true)
				{
					g.setColor(Color.BLACK);
					g.fillRect(x * 100, y * 100, 100, 100);
				}else if(paintbynumber[x][y] == false)
				{
					g.setColor(getBackground());
					g.fillRect(x * 100, y * 100, 100, 100);
				}
			}
		}
		//retrieve state of cell at x y paint panel accordingly
	}

}
