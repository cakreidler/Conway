package conway;

import java.util.Arrays;

public class LifeGrid {
	
	boolean[][] gridA = new boolean[8][8];
	byte[][] gridB = new byte[8][8];
	byte[][] gridC = new byte[8][8];
	
	public boolean[][] getGridA()
	{
		return gridA;
	}
	public byte[][] getGridB()
	{
		return gridB;
	}
	public byte[][] getGridC() 
	{
		return gridC;
	}
	public boolean getGridAloc(byte a, byte b)
	{
		boolean value = gridA[a][b];
		return value;
	}
	public byte getGridBloc(byte a, byte b)
	{
		byte value = gridB[a][b];
		return value;
	}
	public byte getGridCloc(byte a, byte b)
	{
		byte value = gridC[a][b];
		return value;
	}
	public void setGridA(boolean[][] grid)
	{
		this.gridA = Arrays.copyOf(grid, grid.length);
	}
	
	public void setGridB(byte[][] grid)
	{
		this.gridB = Arrays.copyOf(grid, grid.length);
	}
	
	public void setGridC(byte[][] gridC) {
		this.gridC = gridC;
	}
	public void setGridAloc(boolean val, int row, int column)
	{
		this.gridA[row][column] = val;
	}
	
	public void setGridBloc(byte val, int row, int column)
	{
		this.gridB[row][column] = val;
	}
	
	public void setGridCloc(byte val, int row, int column)
	{
		this.gridC[row][column] = val;
	}
	
	public void swapvallocA(byte a, byte b)
	{
		gridA[a][b] = !gridA[a][b];
	}
	public void setGridArow(byte nextByte, int a) {
		// TODO Auto-generated method stub
		for(int i = 0; i < 7; i ++)
		{
			if(nextByte % 2 != 0)
				this.gridA[a][i] = true;
			else
				this.gridA[a][i] = false;
			nextByte = (byte) (nextByte >>> 1);
		}
	}
	
}
