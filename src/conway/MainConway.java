package conway;

import conway.LifeGrid;
import java.util.Scanner;
import conway.Grideye;
import conway.Grideyeimpl;
import javax.swing.*;
import java.awt.*;

public class MainConway {
/**
*	Conway Rules
*	1) Any live cell with fewer than two live neighbors dies, as if caused by under population.  	X < 2
*	2) Any live cell with more than three live neighbors dies, as if by overcrowding.		X > 3
*	3) Any live cell with two or three live neighbors lives on to the next generation.		X = 2 || X = 3
*	4) Any dead cell with exactly three live neighbors becomes a live cell.				X = 3
**/
	public static void main(String[] args) 
	{
		int iterations = 0;
		Scanner scan = new Scanner(System.in);
		LifeGrid land = new LifeGrid();
		Grideye grideye = new Grideyeimpl();
		InfoObj infobj = new InfoObj();
		
		ConwayFrame cframe = new ConwayFrame();
		ConwayDraw biome = new ConwayDraw();
		
		cframe.setSize(800,800);
		cframe.setLocationRelativeTo(null);
		cframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		cframe.setVisible(true);
		cframe.add(biome);

		System.out.println("Generating the Grid");
		//for(int a = 0; a < 8; a++)
		//{
			//System.out.println("Please enter a number between 127 and 0");
			land.setGridArow((byte)(0), 0);
			land.setGridArow((byte)(0), 1);
			land.setGridArow((byte)(0), 2);
			land.setGridArow((byte)(0), 3);
			land.setGridArow((byte)(0), 4);
			land.setGridArow((byte)(8), 5);
			land.setGridArow((byte)(4), 6);
			land.setGridArow((byte)(28), 7);
		//}
		biome.setPaintbynumber(land.getGridA());
		cframe.update();
		System.out.println("Okay, please enter the number of iterations.");
		iterations =  scan.nextInt();
		scan.close();
		System.out.println("Here is the current grid.");
		boolean[][] setup = land.getGridA();
		for(int gridcolumn = 0; gridcolumn < 8; gridcolumn++)
		{
			for(int gridrow = 0; gridrow < 8; gridrow++)
			{
				if(setup[gridrow][gridcolumn] == true)
					System.out.print("T");
				else
					System.out.print("X");
			}
			System.out.println("");
		}
		System.out.println("Let's begin.");
		// TODO Auto-generated method stub
				
		while(iterations > 0)
		{
			for(byte eyey = 1; eyey <= 8; eyey++)
			{
				for(byte eyex = 1; eyex <= 8; eyex++)
				{
					//NUMBER CODES
					//1 = kill this square
					//2 = bring this square to life
					//0 = do nothing
					infobj = grideye.lookatgrid(eyex, eyey, land, infobj);
					land.setGridCloc((byte)(infobj.count), eyex % 8, eyey % 8);
					if ((infobj.status == true) && (infobj.count > 3 || infobj.count < 2))
					{
						land.setGridBloc((byte)(1), eyex % 8, eyey % 8);
					}else if(infobj.status == false && infobj.count == 3)
					{
						land.setGridBloc((byte)(2), eyex % 8, eyey % 8);
					}else
						land.setGridBloc((byte)(0), eyex % 8, eyey % 8);
					//System.out.println(eyey);
				}
				//System.out.println(eyex);
			}
//			byte[][] Bbite = land.getGridB();
//			for(int a = 0; a < 8; a++)
//			{
//			//print grid
//				//System.out.println("Grid B");
//				for(int b = 0; b < 8; b++)
//				{
//					System.out.print(Bbite[b][a]);
//				}
//				System.out.println();
//			}
//			System.out.println("-----");
//			byte[][] Cbite = land.getGridC();
//			for(int a = 0; a < 8; a++)
//			{
//			//print grid
//				//System.out.println("Grid B");
//				for(int b = 0; b < 8; b++)
//				{
//					System.out.print(Cbite[b][a]);
//				}
//				System.out.println();
//			}
			//Now to iterate gridA
			for(byte updatex = 0; updatex <= 7; updatex++)
			{
				for(byte updatey = 0; updatey <= 7; updatey++)
				{
					if(land.getGridBloc(updatex, updatey) == 1)
					{
						land.setGridAloc(false, updatex, updatey);
						land.setGridBloc((byte)(0), updatex, updatey);
					}else if(land.getGridBloc(updatex, updatey) == 2)
					{
						land.setGridAloc(true, updatex, updatey);
						land.setGridBloc((byte)(0), updatex, updatey);
					}else
						land.setGridBloc((byte)(0), updatex, updatey);
				}
			}
			//UPDATE THE FRAME
			biome.setPaintbynumber(land.getGridA());
			cframe.update();
			
			/*
			 * boolean[][] bite = land.getGridA(); for(int a = 0; a < 8; a++) { //print grid
			 * //System.out.println("Grid A"); for(int b = 0; b < 8; b++) { if(bite[b][a] ==
			 * true) System.out.print("T"); else System.out.print("X"); }
			 * System.out.println(); }
			 */
			 
			System.out.println("Next Iteration");
			iterations--;
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("DONE!");
	}	
}

